exports.config = {
  output: './output',
  helpers: {
    REST: {
      endpoint: "http://localhost:81/api/",
      defaultHeaders: {
        'Auth': '11111',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    },
    "ChaiWrapper" : {
      "require": "codeceptjs-chai"
    },
    Playwright: {
      url: 'http://localhost:81',
      show: true,
      browser: 'chromium'
    }
  },
  mocha: {
    "reporterOptions": {
      "mochaFile": "output/result.xml",
      "reportDir": "output"
    }
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    }
    // reportportal: {
    //   projectName: "bLink",
    //   enabled: true,
    //   uuid: "eaec3074-89b3-4d30-bdd1-ba850982140d",
    //   require: '@reportportal/agent-js-codecept',
    //   token: 'eaec3074-89b3-4d30-bdd1-ba850982140d',
    //   endpoint: 'https://demo.reportportal.io/api/v1',
    //   // launchName: 'local launch',
    //   launch: '2heoh_TEST_EXAMPLE',
    //   project: '2heoh_personal'
    // }
  },
  tests: './*_test.js',
  name: 'livedoc',
  translation: 'ru-RU'
}