package BlinkApp;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::SQLite;
use BlinkApp::Service::Link;

sub startup($self) {
    my $config = $self->plugin('NotYAMLConfig');
    $self->secrets($config->{secrets});

    # my $link_service = BlinkApp::Service::Link->new(
    #     ua  => $self->ua,
    #     dbh => Mojo::SQLite->new('sqlite:blink.sqlite3')
    # );


    $self->plugin( 'ServiceContainer', {links => BlinkApp::Service::Link->new(
        ua  => $self->ua,
        dbh => Mojo::SQLite->new('sqlite:blink.sqlite3')
    )} );


    my $r = $self->routes;
    $r->get('/links/')->to('Link#list');
    $r->post('/links/')->to('Link#create');

    # $self->helper(linkservice => $link_service);
}

1