package BlinkApp::Service::Link;
use Mojo::Base -base, -signatures;
use Mojo::UserAgent;

sub new($ref, %args) {
    return bless \%args, $ref
}

sub list($self, %args) {
    retrun $self->{dbh}->db->select('links', [ '*' ], { user_id => $args{user_id} })->hashes;
}

sub create($self, $data) {
    warn 1;
    if (defined($data->{image_url})) {
        my $response = $self->get_image($data->{image_url});

        if ($response->code != 200) {
            $data->{image_url} = undef;
        }

        if (defined $response->headers->content_length && $response->headers->content_length > 1_048_576) {
            $data->{image_url} = undef;
        }
    }

    $data->{id} = $self->insert_link($data);
}

sub get_image($self, $url) {
    # my $ua = Mojo::UserAgent->new;
    return $self->{ua}->get($url)->result;
}

sub insert_link($self, $data) {
    # my $dbh = Mojo::SQLite->new('sqlite:blink.sqlite3');
    return $self->{dbh}->db->insert('links', $data)->last_insert_id()
}

1