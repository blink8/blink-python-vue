use strict;
use warnings;
use Mojo::SQLite;


my $sql = Mojo::SQLite->new('sqlite:blink.sqlite3');
$sql->migrations->name('blinks_app')->from_file('script/db.sql')->migrate;
# -- 1 up
# create table links (id integer primary key autoincrement, title text, url text, description text, image_url text, user_id integer);
# -- 1 down
# drop table links;
# EOF

# $sql->migrations->migrate(0)->migrate;