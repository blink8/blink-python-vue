-- 1 up
create table links (id integer primary key autoincrement, title text, url text, description text, image_url text, user_id integer);
-- 1 down
drop table links;