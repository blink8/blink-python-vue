use Mojo::Base -strict;

use Test::More;
use Test::Mojo;
use Test::Spec;
use Test::Spec::Mocks;

my $t = Test::Mojo->new('BlinkApp', { sqlite => undef });

describe "Link should" => sub {
    my $test_db;
    before sub {
        $test_db = Mojo::SQLite->new('sqlite:test_blink.sqlite3');
        $test_db->migrations->name('blinks_app')->from_file('script/db.sql')->migrate;
        # Mojo::SQLite->expects('new')->at_most_once->returns($test_db);
    my $test_db;
    before sub {
        $test_db = Mojo::SQLite->new('sqlite:test_blink.sqlite3');
        $test_db->migrations->name('blinks_app')->from_file('script/db.sql')->migrate;
        # Mojo::SQLite->expects('new')->at_most_once->returns($test_db);
    };

    after each => sub {
        $test_db->db->query("delete from links");
        $test_db->db->query("delete from sqlite_sequence where name='links';");
        $test_db->db->query("delete from links");
        $test_db->db->query("delete from sqlite_sequence where name='links';");
    };

    after all => sub {
        unlink('test_blink.sqlite3');
        unlink('test_blink.sqlite3-shm');
        unlink('test_blink.sqlite3-wal');
    };

    it "1" => sub {

        my $service = BlinkApp::Service::Link->new(dbh => $test_db);
        say $service->create({ url => 'null' });

    };

    it "has header HTTP_USER_ID " => sub {

        my $data = {
            url => 'https://example.com/',
        };

        $t->post_ok('/links/' => json => $data)
            ->status_is(403)
            ->json_is({ errors => [ { HTTP_USER_ID => 'This header required.' } ] })
    };

    it "has 'description' " => sub {

        my $data = {
            url => 'https://example.com/',
        };

        $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)
            ->status_is(400)
            ->json_is({ errors => [ { description => 'This field may not be null.' } ] })
    };

    it "has HTTP url service should return 400" => sub {
        my $data = {
            url => 'http://example.com/'
        };

        $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)
            ->status_is(400)
            ->json_is({ errors => [ { url => 'should be HTTPS' } ] });
    };

    it " created when image doesn't exists then 'image_url' => undef" => sub {
        my $data = {
            url         => 'https://example.com/',
            title       => 'Example',
            description => 'some description',
            image_url   => 'https://example.com/non-existing-image.png'
        };
        my $res = Mojo::Message::Response->new;
        $res->parse("HTTP/1.0 404 Not found\x0d\x0a");
        Mojo::UserAgent->expects('get')->returns(StubResult->new(result => $res));

        $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)
            ->status_is(200)
            ->json_is({
                id          => 1,
                url         => 'https://example.com/',
                title       => 'Example',
                description => 'some description',
                image_url   => undef,
                user_id     => 1
            });

    };

    # it " created when image size more than 1mb then 'image_url' => undef" => sub {
    #     my $data = {
    #         url         => 'https://example.com/',
    #         title       => 'Example',
    #         description => 'some description',
    #         image_url   => 'https://example.com/big_image.png'
    #     };
    #     my $res = Mojo::Message::Response->new;
    #     $res->parse("HTTP/1.0 200 ОК\x0d\x0a");
    #     $res->headers->add('Content-Length' => 1_048_576 + 1);
    #     Mojo::UserAgent->expects('get')->returns(StubResult->new(result => $res));
    #
    #     $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)
    #         ->status_is(200)
    #         ->json_is({
    #         id          => 1,
    #         url         => 'https://example.com/',
    #         title       => 'Example',
    #         description => 'some description',
    #         image_url   => undef,
    #         user_id     => 1
    #     });
    #
    # };
};

# describe "User can " => sub {
#
#     before sub {
#         my $test_db = Mojo::SQLite->new('sqlite:test_blink.sqlite3');
#         $test_db->migrations->name('blinks_app')->from_file('script/db.sql')->migrate;
#         Mojo::SQLite->expects('new')->at_most_once->returns($test_db);
#     };
#
#     after each => sub {
#         $t->app->sqlite->db->query("delete from links");
#         $t->app->sqlite->db->query("delete from sqlite_sequence where name='links';");
#     };
#
#     after all => sub {
#         unlink('test_blink.sqlite3');
#         unlink('test_blink.sqlite3-shm');
#         unlink('test_blink.sqlite3-wal');
#     };
#
#     it "not get links when not authorised" => sub {
#         $t->get_ok('/links/')->status_is(403);
#     };
#
#     it "create link created without 'image_url'" => sub {
#         my $data = {
#             url         => 'https://example.com/',
#             title       => 'Example',
#             description => 'some description',
#             image_url   => undef
#         };
#
#         $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)
#             ->status_is(200)
#             ->json_is({
#             id          => 1,
#             url         => 'https://example.com/',
#             title       => 'Example',
#             description => 'some description',
#             image_url   => undef,
#             user_id     => 1
#         });
#     };
#
#     sub createLink {
#         my ($t, $data) = @_;
#
#         $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)->status_is(200);
#     }
#
#     it "can get own links " => sub {
#
#         createLink($t, {
#             'title' => 'AgiliX Consulting',
#             'url'=> 'https://agilix.ru',
#             'image_url'=> 'https://example.com/existing-image.png',
#             'description'=> undef
#         });
#         createLink($t, {
#             'title'=> 'Google',
#             'url'=> 'https://google.com',
#             'image_url'=> undef,
#             'description'=> 'null'
#         });
#
#         $t->get_ok('/links/'=> { HTTP_USER_ID => 1 })
#             ->status_is(200)
#             ->json_is('/0/title' =>  'AgiliX Consulting')
#             ->json_is('/1/title' =>  'Google');
#     };
#
#     it "can get own links " => sub {
#         my $data = {
#             'title' => 'AgiliX Consulting',
#             'url'=> 'https://agilix.ru',
#             'image_url'=> 'https://example.com/existing-image.png',
#             'description'=> undef
#         };
#         $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)->status_is(200);
#
#         $data = {
#             'title'=> 'Google',
#             'url'=> 'https://google.com',
#             'image_url'=> undef,
#             'description'=> 'null'
#         };
#         $t->post_ok('/links/' => { HTTP_USER_ID => 2 } => json => $data)->status_is(200);
#
#         $t->get_ok('/links/'=> { HTTP_USER_ID => 1 })
#             ->status_is(200)
#             ->json_is('/0/title' =>  'AgiliX Consulting')
#             ->json_hasnt('/1/title')
#     };
#
#     it "can get links of any user by user_id " => sub {
#         my $data = {
#             'title' => 'AgiliX Consulting',
#             'url'=> 'https://agilix.ru',
#             'image_url'=> 'https://example.com/existing-image.png',
#             'description'=> undef
#         };
#         $t->post_ok('/links/' => { HTTP_USER_ID => 1 } => json => $data)->status_is(200);
#
#         $data = {
#             'title'=> 'Google',
#             'url'=> 'https://google.com',
#             'image_url'=> undef,
#             'description'=> 'null'
#         };
#         $t->post_ok('/links/' => { HTTP_USER_ID => 2 } => json => $data)->status_is(200);
#
#         $t->get_ok('/links/?user_id=2')
#             ->status_is(200)
#             ->json_is('/0/title' =>  'Google')
#             ->json_hasnt('/1/title')
#     };
#
# };

runtests();

package StubResult;

sub new {
    my $ref = shift;
    my %hash = @_;
    return bless \%hash, $ref;
}

sub result {
    return shift->{result};
}

1