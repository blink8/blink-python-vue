# blink

bLink - сервис для обмена полезными ссылками, зарегистрированный пользователь может добавлять ссылки, 
проставлять теги, делиться по короткой ссылке, которую может создать сам или сгенерировать автоматически, 
редактировать название и описание ссылок, голосовать 1-5 звездочек, сервис выводит рейтинг и количество 
проголосовавших

## Perl backend server

## How to setup

```shell
cpan Mojolicious Mojo::SQLite IO::Socket::SSL
```

## How to run server
```shell
perl -Ilib bin/blinkapp.pl
```

## How to run tests
```shell
prove -Ilib
```
