# blink-spa-rest

bLink - сервис для обмена полезными ссылками, зарегистрированный пользователь может добавлять ссылки, проставлять теги, 
делиться по короткой ссылке, которую может создать сам или сгенерировать автоматически, редактировать название и описание 
ссылок, голосовать 1-5 звездочек, сервис выводит рейтинг и количество проголосовавших


## Как запустить с докером

Для запуска сервиса
```shell
$ docker-compose up -d --build
$ docker-compose exec backend python manage.py makemigrations
$ docker-compose exec backend python manage.py migrate
$ docker-compose exec backend python manage.py shell
>>> from django.contrib.auth.models import User
>>> user=User.objects.create_user('edward', password='edward')
>>> user.save()
>>> quit() # :-)
```

Для запуска тестов:
```shell
cd livedoc
npm i

npm run codeceptjs 
```


Компоненты сервиса:
* front           - SPA vue.js

2 варианта backend:
* backend-perl    - REST-сервис на Mojolicious::Lite
* backend-python  - REST-сервис на Django REST framework


## How to run frontend application

service will run on http://localhost:81.

```shell
cd ./frontend
npm i
npm run serve
```

### How to run frontend tests

```shell
npm run test:watch
```
---
## How to setup python-backend

```shell
python3 -m venv env

pip3 install -r requiresource env/bin/activatements.txt
python3 manage.py migrate
```

### How to run server

```shell
python3 manage.py runserver
```

### How to run server tests

```shell
python3 manage.py test
```
---

## How to setup backend-perl

```shell
cpan Mojolicious Mojo::SQLite IO::Socket::SSL
```

### How to run server
```shell
perl -Ilib bin/blinkapp.pl
```

### How to run tests
```shell
prove -Ilib
```