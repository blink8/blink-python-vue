from rest_framework import serializers

from .models import Link


class LinkSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=100)
    description = serializers.CharField()

    class Meta:
        model = Link
        fields = ('title', 'url', 'description', 'image_url', 'created_at', 'updated_at', 'user_id')
