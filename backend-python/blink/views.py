import json
import re

import requests
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .services import LinkService, RequestManager


class LinksView(APIView):
    link_service = LinkService(RequestManager())

    def get(self, request):
        if 'user_id' in request.query_params:
            user_id = request.query_params['user_id']
        else:
            if 'user_id' in request.headers:
                user_id = request.headers['user_id']
            else:
                user_id = None

        try:
            links = self.link_service.get_list_by_user_id(user_id)
        except RuntimeError as error:
            Response({"errors": [error]}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(links)

    def post(self, request):
        if 'user-id' not in request.headers:
            return JsonResponse({"errors": {'HTTP_USER_ID': ['This header required']}},
                                status=status.HTTP_403_FORBIDDEN)

        request.data._mutable = True
        try:
            link = self.link_service.create(request.data, request.headers['user-id'])
        except Exception as error:
            return JsonResponse({"errors": error.args}, status=status.HTTP_400_BAD_REQUEST)

        return JsonResponse(link, status=status.HTTP_201_CREATED)


class PreviewLinkView(APIView):

    def post(self, request):
        try:
            r = requests.get(self.validate_url(request.data.get('url')))
        except ValueError as error:
            return Response({'error': f"Validation: {error}"}, status=status.HTTP_400_BAD_REQUEST)
        except IOError:
            return Response({'error': 'Network: page is unavailable'}, status=status.HTTP_400_BAD_REQUEST)

        if r.status_code == 200:
            return Response({
                'title': self.parse_title(r.text),
                'content-type': r.headers['Content-Type'],
                'image': '/static/screenshots/screenshot.png',
                'description': self.parse_description(r.text),
                'lang': self.parse_lang(r.text)
            }, status=status.HTTP_201_CREATED)

    @staticmethod
    def validate_url(url):
        if not url.startswith("https://"):
            raise ValueError("url should be HTTPS")
        return url

    @staticmethod
    def parse_title(html):
        m = re.search(r'<title>(.+)</title>', html)
        return m.group(1)

    @staticmethod
    def parse_description(text):
        m = re.search(r'<meta property="og:description" content="(.+)" />', text)
        if m is not None and m.group(1) != "":
            return m.group(1)
        else:
            return "no description found"

    @staticmethod
    def parse_lang(html):
        m = re.search(r'<html lang="(.+)"', html)
        if m is not None and m.group(1) != "":
            return m.group(1)
        else:
            return "en"




def get_csrf(request):
    response = JsonResponse({'detail': 'CSRF cookie set'})
    response['X-CSRFToken'] = get_token(request)
    return response


@require_POST
def login_view(request):
    data = json.loads(request.body)
    username = data.get('username')
    password = data.get('password')

    if username is None or password is None:
        return JsonResponse({'detail': 'Please provide username and password.'}, status=400)

    user = authenticate(username=username, password=password)

    if user is None:
        return JsonResponse({'detail': 'Invalid credentials.'}, status=400)

    login(request, user)
    return JsonResponse({'detail': 'Successfully logged in.'})


def logout_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'detail': 'You\'re not logged in.'}, status=400)

    logout(request)
    return JsonResponse({'detail': 'Successfully logged out.'})


@ensure_csrf_cookie
def session_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'isAuthenticated': False})

    return JsonResponse({'isAuthenticated': True})


def whoami_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'isAuthenticated': False})

    return JsonResponse({'username': request.user.username})