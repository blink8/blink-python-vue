import requests

from .models import Link
from .serializers import LinkSerializer


class RequestManager:

    def get_and_check(self, url):
        r = requests.get(url)
        if r.status_code != 200 or int(r.headers['Content-Length']) > 1048576:
            return 'null'
        return url


class LinkService:
    request: RequestManager

    def __init__(self, request):
        self.request = request

    def get_list_by_user_id(self, user_id):
        links = Link.objects.filter(user_id=user_id)
        serializer = LinkSerializer(links, many=True)
        return serializer.data

    def create(self, data, user_id):
        data['user_id'] = user_id
        validate_url(data['url'])
        if data['image_url'] != 'null':
            validate_url(data['image_url'])
            validate_image_url(data['image_url'])
            data['image_url'] = self.request.get_and_check(data['image_url'])

        serializer = LinkSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.data


def validate_url(url):
    if not url.startswith("https://"):
        raise ValueError("url should be HTTPS")
    return url


def validate_image_url(url):
    if not url.endswith("jpg") and not url.endswith("png"):
        raise ValueError("supported image types are: .jpg and png")
    return url
