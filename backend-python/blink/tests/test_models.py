from django.test import TestCase

from ..models import Link


class LinkTest(TestCase):
    def setUp(self):
        Link.objects.create(title="Google", url="https://google.ru")

    def test_link_should_exist(self):
        google = Link.objects.get(title="Google")
        self.assertEqual(google.url, "https://google.ru")
