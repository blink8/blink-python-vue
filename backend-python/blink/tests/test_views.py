import json
from unittest import mock

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status


class CreatePreviewTests(TestCase):

    def test_create_valid_preview(self):
        response = self.client.post(
            reverse('create_preview'),
            data={'url': 'https://agilix.ru/'},
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, status_code, content_length=1):
            self.status_code = status_code
            self.content_length = content_length

    if 'image-is-too-big' in args[0]:
        MockResponse(200, content_length=1048576 + 1)

    if args[0] == 'https://example.com/existing-image.png':
        MockResponse(200)

    return MockResponse(404)


class MyTestClient(Client):

    def __init__(self):
        super().__init__(HTTP_USER_ID=1)


def create_data(title='', url='', image_url='null'):
    return {
        'title': title,
        'url': url,
        'image_url': image_url,
        'description': 'null'
    }


class LinkBuilder(TestCase):

    def __init__(self, url):
        self.url = url

    def WithTitle(self, title):
        self.title = title
        return self

    def ForUser(self, user_id):
        self.user_id = user_id
        return self

    def Build(self, client):
        data = {
            'title': self.title,
            'url': self.url,
            'image_url': 'null',
            'description': 'some description'
        }

        header = {'HTTP_USER_ID': self.user_id}

        client.post(reverse('links_create_retrieve'), data, **header)


class Create:
    def Link(self, url):
        return LinkBuilder(url)


class LinksTests(TestCase):
    client_class = MyTestClient

    def test_user_cannot_create_http_link(self):
        data = create_data(url='http://agilix.ru')

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        body = json.loads(response.content)
        self.assertEqual(body['errors'],  ['url should be HTTPS'])

    def test_user_cannot_create_link_without_description(self):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'null'
        }
        response = self.client.post(reverse('links_create_retrieve'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        body = json.loads(response.content)
        self.assertEqual(body['errors'], [{'description': ['This field is required.']}])

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_create_link(self, stub_get):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'https://example.com/existing-image.png',
            'description': 'null'
        }

        response = self.client.post(reverse('links_create_retrieve'), data=data, **{'HTTP_USER_ID': 1})

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['user_id'], 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_save_link_when_provided_inaccessible_image(self, stub_get):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'https://example.com/non-existing-image.png',
            'description': 'null'
        }

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['image_url'], 'null')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_save_link_when_provided_large_image(self, stub_get):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'https://example.com/image-is-too-big.png',
            'description': 'null'
        }

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['image_url'], 'null')

    def test_user_can_create_link_without_image(self):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'null',
            'description': 'null'
        }
        response = self.client.post(reverse('links_create_retrieve'), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_link_without_user_id_header(self):
        client = Client()
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'null',
            'description': 'null'
        }
        response = client.post(reverse('links_create_retrieve'), data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        body = json.loads(response.content)
        self.assertEqual(body['errors'], {'HTTP_USER_ID': ['This header required']})

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_get_own_links(self, stub_get):
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'image_url': 'https://example.com/existing-image.png',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data)
        data = {
            'title': 'Google',
            'url': 'https://google.com',
            'image_url': 'null',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data)

        response = self.client.get(reverse('links_create_retrieve'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 2)
        self.assertEqual(links[0]['title'], 'AgiliX Consulting')
        self.assertEqual(links[0]['user_id'], 1)
        self.assertEqual(links[1]['title'], 'Google')
        self.assertEqual(links[1]['user_id'], 1)

    def createLink(self, url, title, user):
        data = {
            'title': title,
            'url': url,
            'image_url': 'null',
            'description': 'null'
        }
        self.client.post('/links/', data=data, **{'HTTP_USER_ID': user})

    def test_user_cannot_get_someones_else_links_DSL(self):
        user_id = 1
        self.createLink(url='https://agilix.ru', title='AgiliX Consulting', user=user_id)
        Create().Link('https://google.com').WithTitle('Google').ForUser(2).Build(self.client)

        response = self.client.get('/links/', **{'HTTP_USER_ID': user_id})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 1)
        self.assertEqual(links[0]['title'], 'AgiliX Consulting')
        self.assertEqual(links[0]['user_id'], user_id)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_anyone_can_get_other_users_link_by_id(self, stub_get):
        user_id = 1
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'user_id': user_id,
            'image_url': 'https://example.com/existing-image.png',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data)
        other_user_id = 2
        data = {
            'title': 'Google',
            'url': 'https://google.com',
            'user_id': other_user_id,
            'image_url': 'null',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data, **{'HTTP_USER_ID': other_user_id})
        header = {'HTTP_USER_ID': user_id}

        response = self.client.get('/links/?user_id=2', **header)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 1)
        self.assertEqual(links[0]['title'], 'Google')
        self.assertEqual(links[0]['user_id'], 2)
