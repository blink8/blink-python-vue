from unittest import TestCase
from .. import services


class ValidateURLTests(TestCase):

    def test_url_with_http_should_raise_value_error(self):

        with self.assertRaises(ValueError):
            services.validate_url("http://agilix.ru")

    def test_url_with_http_should_return_it(self):
        url = "https://agilix.ru"

        result = services.validate_url(url)

        self.assertEqual(result, url)
