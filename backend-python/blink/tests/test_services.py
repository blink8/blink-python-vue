from unittest import TestCase
from ..services import RequestManager, LinkService


class LinkServiceTests(TestCase, RequestManager):
    url: str

    def get_and_check(self, url):
        self.url = url
        return url

    def test_when_user_pass_valid_image_url_we_should_fire_get_request(self):
        link_service = LinkService(self)
        data = {
            'url': 'https://agilix.ru',
            'title': 'Agilix Consulting',
            'image_url': 'https://some.valid/image.png',
            'description': 'some description'
        }

        link_service.create(data, 1)

        self.assertEqual(self.url, 'https://some.valid/image.png')
