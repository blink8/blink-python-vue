# blink-python-vue

bLink - сервис для обмена полезными ссылками, зарегистрированный пользователь может добавлять ссылки, 
проставлять теги, делиться по короткой ссылке, которую может создать сам или сгенерировать автоматически, 
редактировать название и описание ссылок, голосовать 1-5 звездочек, сервис выводит рейтинг и количество 
проголосовавших

## How to setup

Требуется python 

```shell
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
python3 manage.py migrate
```

## How to run server
```shell
python3 manage.py runserver
```

## How to run tests
```shell
python3 manage.py test
```
