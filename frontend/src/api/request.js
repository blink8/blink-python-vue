const MAX_RETRIES = 2;
const RETRY_TIMEOUTS = [1000, 5000];

export const sendPost = (url, body, userId) => {
  return new Promise((resolve, reject) => {
    let retry = 0;

    const sendRequest = () => {
      fetch(url, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          USER_ID: userId,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          if (retry < MAX_RETRIES) {
            setTimeout(sendRequest, RETRY_TIMEOUTS[retry]);
            retry++;
          } else {
            reject(error);
          }
        });
    };

    sendRequest();
  });
};
