import { sendPost } from "./request";

const baseURL = "http://127.0.0.1:8000";

export default {
  getPreview: async (url) => {
    const response = await sendPost(`${baseURL}/preview/`, { url });
    if (response.status === 201) {
      return response.json();
    }
    if (response.status === 400) {
      const json = await response.json();
      console.log(json.error);
      throw new Error(json.error);
    }
    throw new Error(`Something went wrong: error ${response.status}`);
  },
  createLink: async (link, userId) => {
    const response = await sendPost(`${baseURL}/link/`, link, userId);
    if (response.status === 201) {
      return response.json();
    }
    if (response.status === 400) {
      const json = await response.json();
      console.log(json.error);
      throw new Error(json.error);
    }
    throw new Error(`Something went wrong: error ${response.status}`);
  },
};
