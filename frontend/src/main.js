import Vue from "vue";
import VueRouter from 'vue-router'
import App from "./App.vue";

import store from "./store";
import Home from './components/HomePage.vue'
import Auth from './components/auth/AuthForm.vue';


Vue.config.productionTip = false
const routes = [
  { path: '/', component: Home },
  { path: '/auth', component: Auth }
]
const router = new VueRouter({
  mode: 'history',
  routes
})

Vue.use(VueRouter);


new Vue({
  render: (h) => h(App),
  store,
  router
}).$mount("#app");

