const state = () => ({
  link: {
    url: "",
    title: "",
    description: "",
    image_url: null,
  },
  loading: false,
});

const getters = {
  validationErrors: (state) => {
    const errors = [];
    if (!state.link.url) {
      errors.push("Please enter a link");
    }
    if (!state.link.url.startsWith("https://")) {
      errors.push("Link should be https");
    }
    if (!state.link.title) {
      errors.push("Please enter a title");
    }
    if (state.link.title.length > 100) {
      errors.push("Title should be less then 100 characters");
    }
    if (!state.link.description) {
      errors.push("Please enter a description");
    }
    if (state.link.description.length > 255) {
      errors.push("Description should be less then 255 characters");
    }
    return errors;
  },

  isValid: (state, getters) => {
    return getters.validationErrors.length === 0;
  },
};

const actions = {
  createLink({ commit, state, getters, rootState }) {
    if (!getters.isValid) {
      return;
    }
    commit("setLoading", true);
    return this.$client
      .createLink(state.link, rootState.user.id)
      .then(() => {
        commit("setLoading", false);
      })
      .catch(() => {
        commit("setLoading", false);
      });
  },
};

const mutations = {
  setUrl(state, url) {
    state.link.url = url;
  },

  setTitle(state, title) {
    state.link.title = title;
  },

  setDescription(state, description) {
    state.link.description = description;
  },

  setLoading(state, loading) {
    state.loading = loading;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
