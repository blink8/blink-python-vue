import Vue from "vue";
import Vuex from "vuex";
import linkBuilder from "./modules/linkBuilder";
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {
      id: null,
      name: "",
    },
  },
  modules: {
    linkBuilder,
  },
});

export default store;
